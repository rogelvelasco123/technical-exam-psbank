import './App.css';
import React, { useState } from 'react';
import { Table } from 'antd';

function App() {
  const [searchItems, setsearchItems] = useState([]);
  const [cartItems, setCartItems] = useState([]);
  const [total, setTotal] = useState(0);
  const [searchValue, setsearchValue] = useState('');

  const array1 = [
    {
      id: 1,
      name: "sample1",
      cost: 10,
      qty: "",
    },
      {
      id: 2,
      name: "sample2",
      cost: 20,
      qty: "",
    },
        {
      id: 3,
      name: "sample3",
      cost: 30,
      qty: "",
    },
  ];
const search = (id) => {
    const a = array1.find(e => e.id == id);
    if(a != undefined){
      setsearchItems(prevArray => [...prevArray, a])
    }

}
console.log(searchItems);

const columns = [
  {
    title: 'Name',
    dataIndex: 'name',
    key: 'name',
  },
  {
    title: 'Cost',
    dataIndex: 'cost',
    key: 'cost',
  },
  {
    title: 'Quantity',
    dataIndex: 'qty',
    key: 'qty',
  },
  {
    title: 'Amount',
    dataIndex: 'amount',
    key: 'amount',
  },
  {
    title: 'Action',
    key: 'action',
    render: (text, record) => (
     
        <a>Delete</a>
    ),
  },
];
const handleAddToCart = (items) => {
  items.map(e => {
    const a = {...e, amount: e.qty * e.cost};
    setCartItems(prev => [...prev, a]);
  })
}
const handleTotal = () =>{
  var total = 0;
  var totalItem = cartItems ? cartItems.map(e => total = total + e.amount) : 0
  return totalItem;
}

  return (
    <div className="App">
      <div className="main">
        <div className="search" style={{marginBottom: "10px"}}>
          <div>Search Product Id:</div>
          <input type="text" onChange={event => setsearchValue(event.target.value)}/>
          <button onClick={()=>search(searchValue)}>Search</button>
        </div>
        { searchItems ? searchItems.map((e, i)=>         
        <div className="product-container" style={{marginBottom: "20px"} } key={e.id}>
          <div className='product'>
            <div style={{marginRight: "5px"}}>Product Id: </div>
            <div>{e.id}</div>
          </div>
          <div className='product'>
            <div style={{marginRight: "5px"}}>Cost: </div>
            <div>{e.cost}</div>
          </div>
          <div className='product'>
            <div style={{marginRight: "5px"}}>Product Name: </div>
            <div>{e.name}</div>
          </div>
          <div className='product'>
            <div style={{marginRight: "5px"}}>Qty: </div> 
            <div>
              <input type="number" onChange={f => {
                e.qty = parseInt(f.target.value, 10)
              }
              }/>
            </div>
          </div>
        </div>
        
        ) : <div></div>}
        {searchItems.length > 0 ?  <button style={{width: "10%"}} onClick={()=>handleAddToCart(searchItems)}>Add to cart</button>: <div></div>}
      </div>
      <h1>OrderList</h1>
      <div className="orderlist">
      <Table columns={columns} dataSource={cartItems} />
      </div>
              <div style={{display: 'flex',flexDirection: 'row'}}>
                   <div>Total Amount:</div>
                   <div>{handleTotal()}</div>
              </div>
    </div>
  );
}

export default App;
